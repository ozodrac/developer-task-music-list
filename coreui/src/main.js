import 'core-js/stable'
import Vue from 'vue'
import App from './App'
import router from './router'
import CoreuiVue from '@coreui/vue'
import { iconsSet as icons } from './assets/icons/icons.js'
import store from './store'
import Toasted from 'vue-toasted'
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import Vuesax from 'vuesax'
import 'vuesax/dist/vuesax.css'
import CKEditor from '@ckeditor/ckeditor5-vue2';
Vue.prototype.$apiAdress = process.env.VUE_APP_BASE_API_URL
Vue.config.performance = true
Vue.use(CoreuiVue)
Vue.use(Toasted)
Vue.use(BootstrapVue)
Vue.use(Vuesax)
// Vue.use(CKEditor)


Vue.toasted.register('login', (message) => message, { type: 'success', position: 'bottom-center', duration: 4000, iconPack: 'fontawesome', icon: 'fingerprint' })
Vue.toasted.register('success', (message) => message, { type: 'success', position: 'bottom-center', duration: 5000 })
Vue.toasted.register('error', message => message, { type: 'error', position: 'bottom-center', duration: 5000 })
Vue.toasted.register('warning', message => message, { type: 'info', position: 'bottom-center', duration: 5000 })

new Vue({
  el: '#app',
  router,
  store,  
  icons,
  template: '<App/>',
  components: {
    App
  },
})
