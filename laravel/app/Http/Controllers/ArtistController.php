<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Albuns;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;

class ArtistController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
            $ch= curl_init();
            curl_setopt($ch, CURLOPT_URL, env('API_URL'));
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_HTTPGET, 1);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(env('API_HEADER')));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            $artists = json_decode(curl_exec($ch));

            return response()->json($artists);
    }

    public function show($id)
    {
        $albuns = Albuns::where('artist_id','=',$id)->get();
        return response()->json($albuns, 200);
    }
    public function albuns($id)
    {
        //
    }


}
