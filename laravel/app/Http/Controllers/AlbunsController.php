<?php

namespace App\Http\Controllers;

use App\Models\Albuns;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;

class AlbunsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function index()
    {
        $albuns = Albuns::all();
        return response()->json($albuns, 200);
    }
    public function filter(Request $request){

        $albuns = Albuns::where('name', 'LIKE', '%' . $request->search . '%')->get();
        return response()->json( compact('albuns') );
    }


    public function store(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'name'      => 'required',
            'artist_id'     => 'required'
        ]);
        if ($validate->fails()){
            return response()->json([
                'status' => 'error',
                'errors' => $validate->errors()
            ], 422);
        }
        $album = new Albuns;
        $album->name = $request->name;
        $album->year = $request->year;
        $album->artist_id = $request->artist_id;
        $album->save();
        return response()->json( ['status' => 'success'] );
    }


    public function show($id)
    {
        $album = Albuns::find($id);
        return response()->json( $album );
    }


    public function update(Request $request, $id)
    {
        $validate = Validator::make($request->all(), [
            'name'      => 'required',
            'artist_id'     => 'required'
        ]);
        if ($validate->fails()){
            return response()->json([
                'status' => 'error',
                'errors' => $validate->errors()
            ], 422);
        }
        $album = Albuns::find($id);
        $album->name        = $request->name;
        $album->year        = $request->year;
        $album->artist_id   = $request->artist_id;
        $album->save();
        return response()->json( ['status' => 'success'] );
    }


    public function destroy($id)
    {
        $albuns = Albuns::find($id);
        if($albuns){
            $albuns->delete();
        }
        return response()->json( ['status' => 'success'] );
    }
}
