<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Albuns extends Model
{
    use HasFactory;
    protected $table = 'albuns';

    protected $fillable = [
        'name','year','artist_id'
    ];

    protected $dates = [
        'deleted_at','created_at','updated_at'
    ];
}
