# Developer Task Music List

[Moat Builders Developer Task ](https://gitlab.com/-/snippets/2167416)

## Installation

1. clone the repo
$ https://gitlab.com/ozodrac/developer-task-music-list

2. Copy file ".env.example", and change its name to ".env".

## Next Step

# in your app directory

# install dependencies
$ docker-compose run --rm php composer install
# generate laravel APP_KEY
$ docker-compose run --rm php php artisan key:generate

# generate jwt secret
$ docker-compose run --rm php php artisan jwt:secret

# run database migration and seed
$ docker-compose run --rm php php artisan migrate:refresh --seed


### When you have project open in browser

Click "Login" on sidebar menu and log in with credentials:

* E-mail: _admin@admin.com_
* Password: _password_

This user has roles: _user_ and _admin_
