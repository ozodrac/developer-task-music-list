# develop stage
FROM node:lts-alpine as dev-stage
WORKDIR /app
COPY ./coreui/package*.json ./
RUN npm install
COPY ./coreui .

# start app
CMD ["npm", "run", "serve"]